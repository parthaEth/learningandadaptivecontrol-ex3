function delta_theta = PI2_Update(Task,batch_sim_out,batch_cost)
%% PI2_Update
% delta_theta = PI2_Update(Task,batch_sim_out,batch_cost)
% 
%   INPUTS:
%       Task: struct with all relevant Task-specific parameters
%             .goal_time:  end time of simulation
%             .start_time: starting time of simulation
%             .dt:         time step for control inputs
%             .max_iteration_learning: maximum number of PI2 iterations
%             .num_rollouts: Number of rollouts for each batch (incl. repeated ones)
%             .num_reuse:    Number of solutions kept after each batch rollout
%             .n_basefct:    Number of base functions for smoothed-time controller
%             .std_noise:    standard deviation of exploration noise for PI2
%
%       batch_sim_out : a "num_rollouts x 1" struct array which contains
%       all rollout simulation data (of the perturbed system) in batch
%       format. It has the following fields
%              .t           vector of discrete simulation times for every
%              rollout
%              .x           state trajectory for every rollout
%              .u           input trajectory for every rollout
%              .Controller  Controller structure. Contains
%                           .BaseFnc(t, x), which
%                           returns a matrix of dimensions
%                           [num_states+1)*n_basefct, length(t)]
%                           indicating the basis function activation at
%                           every t in vector-fashion (one vector per
%                           time-step).
%                           !!! Output needs to be converted to matrix form using
%                           the function vec2mat().
%                           .theta : parameter matrix of dimensions
%                           [(n_gaussians*num_states+1), num_inputs )]
%              .eps         noise for parameter perturbation, of dimensions
%                           [(n_gaussians*num_states+1),num_inputs, num_timesteps]. 
%                           The noise gets reduced automatically as the 
%                           number of rollouts increases. Requires
%                           conversion using vec2mat()
%   
%        batch_cost :      matrix of dimensions (num_rollouts, num_timesteps)
%                           which holds the cost at every timestep for
%                           every rollout.
%               
%   OUTPUTS
%
%   delta_theta


%% Start your implementation here.
% Hint: start by extracting the following quantities

% Number of control inputs
n_u  = size(batch_sim_out(1).u,1);
u = batch_sim_out.u;
%%    
% The total number of parameters per input
n_theta = size(batch_sim_out(1).Controller.theta,1);
% The length of the Upsilone basis function (time-varying basis funciton)
n_gaussian = Task.n_gaussian;
    
% Number of time steps
n_time = size(batch_sim_out(1).t,2);
time= batch_sim_out(1).t;    
% Number of rollouts per iteration
n_rollouts = Task.num_rollouts; % size(batch_sim_out,1);

%Statespace dimension
n_x = size(batch_sim_out(1).x,1);
x = zeros(n_rollouts,n_x,n_time);
%t = 
for k=1:n_rollouts
    x(k,:,:) = batch_sim_out(k).x;
end

cost =  batch_cost;
R = zeros(size(cost));
%alpha = zeros(size(cost));

%%
delta_theta = zeros(195,n_u);
for i = 1:n_u % for each input i
       
% The return of rollouts 
   
<<<<<<< HEAD
for t=1:n_time 
    R(:,t) = cost(:,end);
      for k=1:n_rollouts
      R(k,t) = R(k,t) + cost(k,t);
      end
end
     
=======
% for t=1:n_time-1  
%     R(:,t) = cost(:,end);
%       for k=1:n_rollouts
%       R(k,t) = R(k,t) + cost(k,t);
%       end
% end
for t=1:n_time
    R(:,t) = sum(cost(:,t:end),2);
end
    
    
    
    
>>>>>>> b64edf6ccbd4d4c5bb2bd51caeee36074e24836b
%% The exponentiated cost calculation is given
% compute the exponentiated cost with the special trick to automatically
% adjust the lambda scaling parameter

minR = repmat( min(R,[],1), [n_rollouts 1]);
maxR = repmat( max(R,[],1), [n_rollouts 1]);
medR = repmat( median(R,1), [n_rollouts 1]);

% \exp(-1/lambda R)
expR = exp(-10*(R-minR)./(maxR-minR));


%% continue here with computing alpha
alpha =  expR./repmat(sum(expR,2),[1 n_time]);   
    
delta_theta_mat = zeros(195,n_time,n_u);
for k=1:n_rollouts   
    T = (batch_sim_out(k).Controller.BaseFnc(time,mat2vec(x(k,:,:)))); 
    for s=1:n_time 
        eps = batch_sim_out(k).eps(:,i,s);
<<<<<<< HEAD
%         T1 = T(1:13:195,s)*T(1:13:195,s)';
%         T2 = T(1:13:195,s)'*T(1:13:195,s);
%         delta_theta_mat(:,s,i) = delta_theta_mat(:,s,i)+reshape(alpha(k,s)*T1*vec2mat(eps)/T2,195,1);
        T1 = T(:,s)*T(:,s)';
        T2 = T(:,s)'*T(:,s);
        delta_theta_mat(:,s,i) = delta_theta_mat(:,s,i)+alpha(k,s)*T1*eps/T2;
=======
        T1 = T(1:13:195,s)*T(1:13:195,s)';
        T2 = T(1:13:195,s)'*T(1:13:195,s);
        delta_theta_mat(:,s,i) = delta_theta_mat(:,s,i)+reshape(alpha(k,s)*T1*vec2mat(eps)/T2,195,1);
>>>>>>> b64edf6ccbd4d4c5bb2bd51caeee36074e24836b
    end
end
%delta_theta_i = reshape(sum(delta_theta_mat(:,:,i).*repmat(T(1:13:195,:),13,1),2),195,1)./sum(repmat(T(1:13:195,:),13,1),2);
delta_theta_i = zeros(195,1);
for s=1:n_time 
    delta_theta_i = delta_theta_i+delta_theta_mat(:,s,i).*T(:,s);     
end
delta_theta_i = delta_theta_i./sum(T,2);
    %% conversion back to vector-style
    delta_theta(:,i) = delta_theta_i;
    
end

%% additional functions vec2mat and mat2vec that are provided
function mat = vec2mat(vec)
    % Moving the time dimension (2nd dimension) to 3rd dimension
    % if the we have just one time instance, this will not change the 
    % dimension of vec
    vec = permute(vec, [1 3 2]);
    
    mat = reshape(vec,[],Task.n_gaussian,size(vec,3));
    mat = permute(mat, [2 1 3]);
end


function vec = mat2vec(mat)
    
    mat = permute(mat, [2 1 3]);
    vec = reshape(mat,[],1,size(mat,3));
end


end